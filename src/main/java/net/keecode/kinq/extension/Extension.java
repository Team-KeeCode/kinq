package net.keecode.kinq.extension;

import java.io.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by Michael on 23.04.2016.
 */
public abstract class Extension {

	public abstract Object where(Object object, Predicate filter);

	public abstract Object select(Object object, String field);

	public abstract boolean any(Object object, Predicate<Object> filter);

	public abstract boolean all(Object object, Predicate<Object> filter);

	public abstract void forEach(Object object, Consumer<Object> consumer);

	protected Object clone(Object objSource) {
		Object objDest = new Object();
		try {
			ByteArrayOutputStream bos;
			bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(objSource);
			oos.flush();
			oos.close();
			bos.close();
			byte[] byteData = bos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
			try {
				objDest = new ObjectInputStream(bais).readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return objDest;

	}
}
