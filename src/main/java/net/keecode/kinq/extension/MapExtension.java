package net.keecode.kinq.extension;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by Michael on 23.04.2016.
 */
public class MapExtension extends Extension {

	private boolean sortValues = false;

	public MapExtension() {
	}

	public MapExtension(boolean sortValues) {
		this.sortValues = sortValues;
	}

	@Override
	public Map where(Object object, Predicate filter) {

		Map map = ((Map) object);
		List keys = new ArrayList(map.keySet());
		List values = new ArrayList(map.values());

		Map matches = (Map) clone(object);
		matches.clear();

		if (sortValues) {
			for (int i = 0; i < map.values().size(); i++) {
				if (filter.test(keys.get(i)))
					matches.put(keys.get(i), values.get(i));
			}
		} else {
			map.keySet().stream().filter(filter::test).forEach(o -> matches.put(o, map.get(o)));
		}

		return matches;
	}

	@Override
	public Collection select(Object object, String field) {
		Map map = ((Map) object);
		List keys = new ArrayList(map.keySet());
		List values = new ArrayList(map.values());

		Collection fieldContent = new ArrayList();

		if (sortValues) {
			for (Object value : values) {
				try {
					Field f = value.getClass().getField(field);
					f.setAccessible(true);
					fieldContent.add(f.get(value));
				} catch (IllegalAccessException | NoSuchFieldException e) {
					e.printStackTrace();
				}
			}
		} else {
			for (Object key : keys) {
				try {
					Field f = key.getClass().getField(field);
					f.setAccessible(true);
					fieldContent.add(f.get(key));
				} catch (IllegalAccessException | NoSuchFieldException e) {
					e.printStackTrace();
				}
			}
		}
		return fieldContent;
	}

	@Override
	public boolean any(Object object, Predicate<Object> filter) {
		return where(object, filter).size() > 0;
	}

	@Override
	public boolean all(Object object, Predicate<Object> filter) {
		if (sortValues)
			return where(object, filter).size() == ((Map) object).values().size();
		else
			return where(object, filter).size() == ((Map) object).keySet().size();
	}

	@Override
	public void forEach(Object object, Consumer<Object> consumer) {
		Map map = ((Map) object);
		List keys = new ArrayList(map.keySet());
		List values = new ArrayList(map.values());

		if (sortValues) {
			values.forEach(consumer::accept);
		} else {
			keys.forEach(consumer::accept);
		}
	}

}
