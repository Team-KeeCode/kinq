package net.keecode.kinq.extension;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by Michael on 22.04.2016.
 */
public class CollectionExtension extends Extension {

	@Override
	public Collection where(Object object, Predicate filter) {
		Collection collection = (Collection) object;
		Collection match = new ArrayList<>();

		collection.stream().filter(filter).forEach(match::add);

		return match;
	}

	@Override
	public Collection select(Object object, String field) {
		Collection collection = (Collection) object;
		Collection<Object> match = new ArrayList<>();
		for (Object value : collection) {
			try {
				Field f = value.getClass().getField(field);
				f.setAccessible(true);
				match.add(f.get(value));
			} catch (IllegalAccessException | NoSuchFieldException e) {
				e.printStackTrace();
			}
		}

		return match;
	}

	@Override
	public boolean any(Object object, Predicate<Object> filter) {
		return where(object, filter).size() > 0;
	}

	@Override
	public boolean all(Object object, Predicate<Object> filter) {
		return where(object, filter).size() == ((Collection) object).size();
	}

	@Override
	public void forEach(Object object, Consumer<Object> consumer) {
		Collection collection = (Collection) object;
		collection.forEach(consumer);
	}
}
