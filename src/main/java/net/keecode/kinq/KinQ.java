package net.keecode.kinq;

import net.keecode.kinq.extension.CollectionExtension;
import net.keecode.kinq.extension.MapExtension;

import java.util.Collection;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author Team KeeCode
 * @version 1.0
 *          <p>
 *          Easily filter your Lists
 */
public class KinQ {

	private MapExtension mapExtension;
	private CollectionExtension collectionExtension;

	private Collection list;
	private Map map;

	/**
	 * Create a sorter for the provided list
	 *
	 * @param list The list you want to use KinQ on
	 */
	public KinQ(Collection list) {
		this.list = list;
		collectionExtension = new CollectionExtension();
	}

	/**
	 * Create a sorter for the provided map
	 *
	 * @param map The map you want to use KinQ on
	 */
	public KinQ(Map map) {
		this.map = map;
		mapExtension = new MapExtension();
	}

	/**
	 * Create a sorter for the provided map
	 *
	 * @param map        The map you want to use KinQ on
	 * @param sortValues Indicates if the values should be sorted instead of the keys
	 */
	public KinQ(Map map, boolean sortValues) {
		this.map = map;
		mapExtension = new MapExtension(sortValues);
	}

	/**
	 * Filter the list according to your filter
	 *
	 * @param type   The type of your list (For combatibility)
	 * @param filter A lambda Filter
	 * @return The KinQ instance
	 */
	public <T> KinQ where(Class<T> type, Predicate<T> filter) {
		return where(filter);
	}

	/**
	 * Filter the list according to your filter
	 *
	 * @param filter A Lambda Filter
	 * @return The KinQ Instance
	 */
	public KinQ where(Predicate filter) {
		if (list != null)
			list = collectionExtension.where(list, filter);
		else
			map = mapExtension.where(map, filter);
		return this;
	}

	/**
	 * Get a list of values stored in the specific field in the objects stored in the list
	 *
	 * @param type  The type of your list (For combatibility)
	 * @param field The field in the object
	 * @return The KinQ instance
	 */
	public <T> KinQ select(Class<T> type, String field) {
		return select(field);
	}

	/**
	 * Get a list of values stored in the specific field in the objects stored in the list
	 *
	 * @param field The field in the object
	 * @return The KinQ instance
	 */
	public KinQ select(String field) {
		if (list != null)
			list = collectionExtension.select(list, field);
		else
			list = mapExtension.select(map, field);
		return this;
	}

	/**
	 * Check if any object in the list meets the provided filter
	 *
	 * @param filter The filter
	 * @param type   The type of your list (For combatibility)
	 * @return true if requirements met
	 */
	public <T> boolean any(Class<T> type, Predicate<T> filter) {
		return any(filter);
	}

	/**
	 * Check if any object in the list meets the provided filter
	 *
	 * @param filter The filter
	 * @return true if requirements met
	 */
	public boolean any(Predicate filter) {
		if (list != null)
			return collectionExtension.any(list, filter);
		else
			return mapExtension.any(map, filter);
	}

	/**
	 * Check if all object in the list meet the provided filter
	 *
	 * @param filter The filter
	 * @param type   The type of your list (For combatibility)
	 * @return true if requirements met
	 */
	public <T> boolean all(Class<T> type, Predicate<T> filter) {
		return all(filter);
	}

	/**
	 * Check if all object in the list meet the provided filter
	 *
	 * @param filter The filter
	 * @return true if requirements met
	 */
	public boolean all(Predicate filter) {
		if (list != null)
			return collectionExtension.all(list, filter);
		else
			return mapExtension.all(map, filter);
	}

	/**
	 * @param type     The type of your list (For combatibility)
	 * @param consumer The action in lambda expression
	 * @return The KinQ instance
	 */
	public <T> KinQ forEach(Class<T> type, Consumer<T> consumer) {
		forEach(consumer);
		return this;
	}

	/**
	 * Execute an action for every object in the list
	 *
	 * @param consumer The action in lambda expression
	 * @return The KinQ instance
	 */
	public KinQ forEach(Consumer consumer) {
		if (list != null)
			collectionExtension.forEach(list, consumer);
		else
			mapExtension.forEach(map, consumer);
		return this;
	}

	/**
	 * Get your filtered collection
	 *
	 * @param type The type of your list (For combatibility)
	 * @return The filtered list
	 */
	public <T> Collection<T> toList(Class<T> type) {
		return (Collection<T>) toList();
	}

	/**
	 * Get your filtered collection
	 *
	 * @return The filtered list
	 */
	public Collection toList() {
		return list;
	}

	/**
	 * Get your filtered map
	 *
	 * @return The filtered map
	 */
	public Map toMap() {
		return map;
	}

}
