package net.keecode.kinq;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Michael on 22.04.2016.
 */
public class KinQTest {

	private Collection<TestObject> list;

	@org.junit.Before
	public void setUp() throws Exception {
		list = new ArrayList<>();
	}

	@org.junit.Test
	public void where() throws Exception {

	}

	@org.junit.Test
	public void where1() throws Exception {

	}

	@org.junit.Test
	public void select() throws Exception {

	}

	@org.junit.Test
	public void any() throws Exception {
		KinQ sorter = new KinQ(list);
		list.add(new TestObject());
		list.add(new TestObject());
		assertTrue(sorter.any(TestObject.class, to -> to.test.equals("Test")));
		assertFalse(sorter.any(TestObject.class, to -> to.test.equals("Te2st")));
	}

	@org.junit.Test
	public void all() throws Exception {
		KinQ sorter = new KinQ(list);
		list.add(new TestObject());
		list.add(new TestObject());
		assertTrue(sorter.all(TestObject.class, to -> to.test.equals("Test")));
		assertFalse(sorter.all(TestObject.class, to -> to.test.equals("Te2st")));
	}

	@org.junit.Test
	public void forEach() throws Exception {
		KinQ sorter = new KinQ(list);
		list.add(new TestObject());
		list.add(new TestObject());
		sorter.forEach(TestObject.class, to -> assertTrue(to.test.contains("Test")));
	}

	@org.junit.Test
	public void forEach1() throws Exception {
		KinQ sorter = new KinQ(list);
		list.add(new TestObject());
		list.add(new TestObject());
		sorter.forEach(to -> assertTrue(((TestObject) to).test.contains("Test")));
	}

	@org.junit.Test
	public void toList() throws Exception {

	}

	@org.junit.Test
	public void toList1() throws Exception {

	}

}